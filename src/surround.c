#include "surround.h"

/*Get the current line and count the column number
 * return 1 if good otherwise return 0*/
int isGoodColumn(char* line)
{
  int cur_col, i = 0;
  while(i < MAXCOL || line[i] != '\n')
  {
    cur_col++;
    ++i;
  }
  return (cur_col >= MINCOL) ? 1 : 0;
}

/*insert all the data from the file, one by one, into the given 2D array*/
void line_to_array(char* line, char* array[], int nbl)
{
  int cur = 0;
  while(line[cur] != '\n')/*here the length shall be the same as maxcol*/
  {
    array[nbl][cur] = line[cur];
    cur++;
  }
}

/*get the total number of line in the given file
 * Check also the file format of the line and the colone
 * RETURN nb_line if good ELSE 0*/
int read_map(const char* fn, char* array[])
{
  FILE *file = fopen(fn,"rb");
  int nb_line = 0;
  if (file != NULL)
  {
    char line[MAXLINE];
    while (fgets(line, sizeof(line), file) != NULL) /* read a line */
    {
      /*Get if there is a good number of column*/
      if (!isGoodColumn(line))
        return 0;
      /*fill the array*/
      line_to_array(line, array, nb_line);
      nb_line++; /* increment nb_line */
    }
    fclose (file);
  }
  else
  {
    perror(fn); /* error occured */
    return 0;
  }

  /*check error format of the given file*/
  if (nb_line >= MINLINE)
    return 1;
  else
    return 0;
}

void display_welcome(char* file)
{
  printf("***********************************\n");
  printf("*WELCOME TO THE SURROUNDED PROGRAM*\n");
  printf("***********************************\n");
  printf("\n\n");
  printf("...\n");
  sleep(3);
  printf("You choose to analyse %s file.\n", file);
  printf("Beginning process now...\n");
  sleep(4);
}

void display_array(char* array[])
{
  for(int i = 0; i < MAXCOL; i++)
  {
    for(int j = 0; j < MINLINE; j++)
      printf("%c", array[i][j]);
    printf("\n");
  }
  printf("\n");
}


/*check  the data to analyze any surrounded target point through the array*/
void find_surr(char* array[])
{
  int ext = 0;/*to detect outside point*/
  int cur = 0;/* get the currents number of surrounded cases*/
  int tot = (MINLINE * MINCOL) - 1;/* -1 FOR the middle one */
  for(int i = 0; i < MAXCOL; i++)
  {
    for(int j = 0; j < MAXLINE; j++)
    {
      /*check if the current point is different from the surrounded one*/
      if(array[i][j] == TARGET)
      {
        /*analyze by square of MINCOL * MINLINE*/
        for(int k = (int)floor(i - (MINCOL / 2)); k < MINCOL; k++)
        {
          for(int l = (int)floor(j - (MINLINE / 2)); l < MINLINE; l++)
          {
            if( k > 0 && l > 0 && (k != i && l != j) && array[k][l] ==  SURROUND)
              cur++;
            else
              ext++;
          }
        }
        /*check if we have a surrounded target :*/
        if(cur == (tot - ext))
        {
          /*run fill square*/
          fill_square(i, j, array);
          /*display new array*/
          printf("A cell has been surrounded.\n");
          display_array(array);
          /*move ahead the current values of i and j from MINCOL and MINLINE */
          i += MINCOL;
          j += MINLINE;
        }
        /*re-init case variable*/
        ext = 0;
        cur = 0;
      }
    }
  }
}



/*fill the found surrounded cell*/
void fill_square(int i, int j, char* array[])
{

  for(int k = (int)floor(i - (MINCOL / 2)); k < MINCOL; k++)
  {
    for(int l = (int)floor(j - (MINLINE / 2)); l < MINLINE; l++)
    {
      if(k > 0 && l > 0)
        array[k][l] = TARGET;
    }
  }
}

/*RETURN 1 IF IS GOOD 0 ELSEWHERE*/
/*int check_map(const char* file_name, char* array[])
{
  FILE *fh = fopen(file_name, "rb");//rb == r
  int i, j = 0;//go through the array

  if (fh != NULL)
  {
    char line[MAXLINE];
    while(fgets(line, sizeof(line), fh) != NULL)
    {
      while(*line)
      {
        if(*line != array[i][j])
          return 0;
        line++;
        j++;
      }
      j = 0;
      i++;
    }
    fclose(fh);
  }
  return 1;
}*/

void init_array(char** array)
{
  for(int i = 0; i < MAXLINE; i++)
  {
    array[i][MAXCOL] = '\n';
  }
}

int main(int arg, char* argv[])
{
  /*avoid warning*/
  arg = arg + 0;
  /*Init parameters*/
  char** array;
  array = malloc(sizeof(sizeof(char) * MAXLINE * MAXCOL));
  init_array(array);
  /*Welcome msg*/
  display_welcome(argv[1]);
  /*Load the file into an array*/
  if(read_map(argv[1], array) == 0)
    return 1;
  else
  {
    display_array(array);
    find_surr(array);
  }
  return 0;
}
