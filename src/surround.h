#ifndef SURROUND_H
# define SURROUND_H
/*dependency library*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

/*MACCRO variables here*/
#define MAXLINE 512
#define MAXCOL  512

#define MINLINE   3
#define MINCOL    3

#define SURROUND 'X'
#define TARGET   'O'

int isGoodColumn(char* line);

void line_to_array(char* line, char* array[], int nb_line);

int read_map(const char* fn, char* array[]);

void display_welcome(char* file);
void display_array(char* array[]);

void find_surr(char* array[]);
void fill_square(int i, int j, char* array[]);

void init_array(char** array);

//int check_map(const char* fn, char* array[]);

#endif /* !SURROUND_H  */
