CC       = gcc
CFLAGS   = -Wall -Wextra -Werror -std=c99 -pedantic
SRC      = src/file1.c src/file2.c \
OBJ      = $(SRC:.c=.o)

TSRC      = check/file1.c check/file2.c
TOBJ      = $(TSRC:.c=.o)

.PHONY: projectname
.PHONY: test

projectname: $(OBJ)
	$(CC) $(CFLAGS) $^ -o $@

test1: $(TOBJ)
	$(CC) $(CFLAGS) $^ -o $@

clean:
	$(RM) -f $(OBJ) $(TOBJ) *.swp *.*~
